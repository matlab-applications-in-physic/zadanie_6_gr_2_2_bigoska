path = 'C:\Users\student\Desktop\MatlabGos\jsonlab-1.9\jsonlab';
addpath(path);

katowice=loadjson(urlread ('https://danepubliczne.imgw.pl/api/data/synop/station/katowice/format/json'));
czestochowa=loadjson(urlread ('https://danepubliczne.imgw.pl/api/data/synop/station/czestochowa/format/json'));
bielskobiala=loadjson(urlread ('https://danepubliczne.imgw.pl/api/data/synop/station/bielskobiala/format/json'));

plik = 'weatherData.csv';
header = {'Stacja', 'Data', 'Godzina', 'Temperatura', 'Cisnienie', 'Predkosc wiatru',...
    'Wilgotnosc', 'Temperatura odczuwalna'};
header_1 = {' ', ' ', ' ', '�C', 'hPa', 'km/h', '%', '�C'};
data = fopen(plik, 'w+');
fprintf(data, '%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s\n','', header{1}, ';', header{2}, ';', header{3}, ';', header{4}, ';', header{5}, ';', header{6}, ';', header{7},';', header{8}, '');
fprintf(data, '%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s\n', '', header_1{1}, ';', header_1{2}, ';', header_1{3}, ';', header_1{4}, ';', header_1{5}, ';', header_1{6}, ';', header_1{7}, ';', header_1{8}, '');
fclose(data)

Katowice=cell(7,2)

Katowice{1,2} = katowice.data_pomiaru;
Katowice{2,2} = str2num(katowice.godzina_pomiaru);
Katowice{3,2} = str2num(katowice.temperatura);
Katowice{4,2} = str2num(katowice.cisnienie);
Katowice{5,2} = str2num(katowice.predkosc_wiatru);
Katowice{6,2} = str2num(katowice.wilgotnosc_wzgledna);
%Katowice{7,2} = 13.12+(0.6215*Katowice{3,2})-(11.37*(Katowice{5,2}^0.16))+(0.3965*Katowice{3,2}*(Katowice{5,2}^0.16));
%dlmwrite(plik, array_k, '-append', 'delimiter', ';');
%file_name='weatherData.csv'
%plik = fopen(file_name, 'a');
Czestochowa=cell(7,2)

Czestochowa{1,2} = czestochowa.data_pomiaru;
Czestochowa{2,2} = str2num(czestochowa.godzina_pomiaru);
Czestochowa{3,2} = str2num(czestochowa.temperatura);
Czestochowa{4,2} = str2num(czestochowa.cisnienie);
Czestochowa{5,2} = str2num(czestochowa.predkosc_wiatru);
Czestochowa{6,2} = str2num(czestochowa.wilgotnosc_wzgledna);

BielskoBiala=cell(7,2)
BielskoBiala{1,2} = bielskobiala.data_pomiaru;
BielskoBiala{2,2} = str2num(bielskobiala.godzina_pomiaru);
BielskoBiala{3,2} = str2num(bielskobiala.temperatura);
BielskoBiala{4,2} = str2num(bielskobiala.cisnienie);
BielskoBiala{5,2} = str2num(bielskobiala.predkosc_wiatru);
BielskoBiala{6,2} = str2num(bielskobiala.wilgotnosc_wzgledna);

data = fopen(plik, 'a');
fprintf(data,'%s%s%s%s%s%f%s%f%s%f%s%f%s%f%s\n','','Katowice',';',Katowice{1,2},';',Katowice{2,2},';',Katowice{3,2},';',Katowice{4,2},';',Katowice{5,2},';',Katowice{6,2},'')
fprintf(data, '%s%s%s%s%s%f%s%f%s%f%s%f%s%f%s\n','','Czestochowa',';',Czestochowa{1,2},';',Czestochowa{2,2},';',Czestochowa{3,2},';',Czestochowa{4,2},';',Czestochowa{5,2},';',Czestochowa{6,2},'');
fprintf(data, '%s%s%s%s%s%f%s%f%s%f%s%f%s%f%s\n','','BielskoBiala',';',BielskoBiala{1,2},';',BielskoBiala{2,2},';',BielskoBiala{3,2},';',BielskoBiala{4,2},';',BielskoBiala{5,2},';',BielskoBiala{6,2},'');
fclose(data)